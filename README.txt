                            recfla.sh:
         A Command-Line REPL Alternative for Flash Cards

'recfla.sh' is a flashcards solution written in bash, that uses GNU
Recutils to store information. It aims to be a minimalist option to
flashcard storing, reviewing, querying and also sharing.

The reviewing system is a simple 3-box Leitner system.

§ Notice!

This software is in its early days of development. It is already
usable, but a lot of testing and loose ends tying-up is still needed
before a first release.

§ Dependencies

'recfla.sh' depends on: GNU Coreutils, GNU Recutils, GNU Bash, GNU sed,
GNU awk and fzf.

In a Debian-based system, the command for installing such dependencies
would be:

{
    # apt install coreutils recutils bash sed gawk fzf
}

If you plan to store math equations, is recommended to install 'flatlatex'
in order to convert LaTeX math to unicode text.

§ License

Copyright (C) 2022 Guilherme H. Jardim.
License GPLv3+: GNU GPL version 3 or later:
https://gnu.org/licenses/gpl.html.
