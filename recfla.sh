#!/usr/bin/env bash
# shellcheck disable=SC2086

# TODO: Add error messages
# TODO: Work with exit statuses
# TODO: In review, check if there's cards included
# TODO: Trim SideA and SideB before inserting
# TODO: Loop card insertion

sysconfdir="/usr/local/etc"
[ -e "$XDG_CONFIG_HOME/recfla.sh/recflashrc" ] || {
    cp ${sysconfdir}/recfla.sh/{empty.rec,recflashrc} \
        "$XDG_CONFIG_HOME/recfla.sh/"
}
source "$XDG_CONFIG_HOME/recfla.sh/recflashrc" || exit 1

# Basic functions and variables -------------------------------------------------------------------{{{
## Variables
lastcycle=$(recsel -t ReviewingCycle -P LastCycle "$flashcardsfile")

## Functions
boldprint(){ printf "%b" "$1$2"; }
bold_echo(){ printf "%b" "$1 $2\n"; }
get_multiline_text(){ sed '/^$/q'; }

cards_in_deck(){
    if [[ "$2" == "--collapse" ]]; then
        local cards s r
        s=$'\n+' r=$'\u2028'

        cards=$(recsel -t Card -p SideA -S SideA -C -e "Deck = '$1'" "$flashcardsfile")
        cards="${cards//$s/$r}"
        cards="${cards//SideA: /}"
        echo "$cards"
    else
        recsel -t Card -P SideA -S SideA -C -e "Deck = '$1'" "$flashcardsfile"
    fi
}

deckcount(){ recsel -t Deck -c "$flashcardsfile"; }
is_there_root_decks(){
    [[ $(echo_topdecks) ]] || {
        echo -e "\nNo root decks found."
        return 1
    }
}

## Exports (these need to be accessible for fzf)
export -f cards_in_deck
export flashcardsfile
# -------------------------------------------------------------------------------------------------}}}
# Deck search -------------------------------------------------------------------------------------{{{
echo_topdecks(){
    recsel -e "#Superdeck = 0" -t Deck "$flashcardsfile" | awk -F': ' '!/^$/ {print $2 }'
}

echo_subdecks(){
    recsel -e "Superdeck = '$1'" -P Name -t Deck "$flashcardsfile" | sed '/^$/d'
}

recursive_echo_decks(){
    local sel="$1"
    local prefix="$2"
    while read -r deck; do
        echo "$prefix$deck"

        newsel=$(echo_subdecks "$deck")
        [[ $newsel ]] && recursive_echo_decks "$newsel" "$prefix  "
    done <<< "$sel"
}

decksearch(){
    is_there_root_decks || return 1

    # Flag handling
    prompt='Choose a deck:'
    while getopts 'a:p:' flag; do
        case "${flag}" in
            a) newoptions+=("$OPTARG") ;;
            p) prompt="${OPTARG}" ;;
            *) return 1 ;;
        esac
    done

    sel=$(echo_topdecks)
    {
        for newoption in "${newoptions[@]}"; do
            echo "$newoption"
        done
        recursive_echo_decks "$sel" ""
    } | fzf --algo=v1 \
            --reverse \
            --header="$prompt" \
            --preview "cards_in_deck '{1..}' --collapse" \
      | sed 's/^\s*//'
}
# -------------------------------------------------------------------------------------------------}}}
# Deck insert -------------------------------------------------------------------------------------{{{
deck_first_insert(){
    boldprint "Add new deck name: "
    read -r deckName
    if recins --verbose -t Deck \
        -f Name -v "$deckName" \
        "$flashcardsfile"
    then
        echo "Added: $deckName"
    else
        return 1
    fi
}

deck_single_insert(){
    boldprint "Add new deck name: "
    read -r deckName

    parentdeck=$(decksearch \
        -a "No parent deck." \
        -p "Select parent deck for '$deckName':")

    if
        if [ "$parentdeck" = "No parent deck." ]
        then
            recins --verbose -t Deck \
                -f Name -v "$deckName" \
                "$flashcardsfile"
        else
           recins --verbose -t Deck \
                -f Name -v "$deckName" \
                -f Superdeck -v "$parentdeck" \
                "$flashcardsfile"
        fi
    then
       echo "Added: $deckName"
    else
        return 1
    fi
}

deck_multiple_insert(){
    (is_there_root_decks > /dev/null) || {
        echo " > This is your first root deck."
        if deck_first_insert
        then
            return 0
        else
            echo ''
            echo ' > Perhaps you did something wrong. Please try again, if you wish.'
            return 1
        fi
    }

    echo " > Notice: deck insertion must be top-down"

    deck='New deck…'
    while [ "$deck" == 'New deck…' ]
    do
        p=0

        boldprint "New deck: "
        deck="$(deck_single_insert)" || {
            echo ''
            echo ' > Perhaps you did something wrong. Please try again, if you wish.'
            break
        }

        deck="$(echo $deck \
            | tail -n 1 \
            | sed 's/Add new deck name: Added: //')"

        multinsert_prompt[0]=' > Please type [y/n]: '
        multinsert_prompt[1]=" > Deck '$deck' inserted.
 > Add another one? [y/n]: "
        unset ans
        until [[ ${ans,,} == [yn] ]]; do
            read -r -p "${multinsert_prompt[!p++]}" ans
        done

        [[ ${ans,,} == y ]] && deck='New deck…'
    done
}

# -------------------------------------------------------------------------------------------------}}}
# Deck move ---------------------------------------------------------------------------------------{{{
# NOTE: is working with 'No parent deck.' a good way to handle this?
# There may be a deck with that name. It is not prohibited in the Record specification.

deckmove(){
    is_there_root_decks || return 1
    [ "$(deckcount)" -eq "1" ] && {
        echo "You have only one deck. Please insert more decks to move."
        return 1
    }

    selected_deck=$(decksearch -p 'Which deck you want to move (origin)?')
    old_parent_deck=$(recsel -t Deck -e "Name = '$selected_deck'" -P Superdeck "$flashcardsfile")
    [ "$old_parent_deck" ] || old_parent_deck='No parent deck.'
    new_parent_deck=$(decksearch \
        -a 'No parent deck.' \
        -p "Which deck you want to be the parent deck of '$selected_deck'?")

    echo "$selected_deck: $old_parent_deck →  $new_parent_deck"
    if [ "$old_parent_deck" = "$new_parent_deck" ]
    then
        echo "New parent deck is the same as old parent deck. Doing nothing."
    else
        if [ "$old_parent_deck" = 'No parent deck.' ]
        then
            recset -t Deck -e "Name = '$selected_deck'" \
                -f Superdeck \
                -a "$new_parent_deck" \
                "$flashcardsfile"
        else
            if [ "$new_parent_deck" = 'No parent deck.' ]
            then
                recset -t Deck -e "Name = '$selected_deck'" \
                    -f Superdeck \
                    -d \
                    "$flashcardsfile"
            else
                recset -t Deck -e "Name = '$selected_deck'" \
                    -f Superdeck \
                    -s "$new_parent_deck" \
                    "$flashcardsfile"
            fi
        fi
    fi
}
# -------------------------------------------------------------------------------------------------}}}
# Card insert -------------------------------------------------------------------------------------{{{
# TODO: Check if SideA = ''
# TODO: Check if Deck = ''
cardinsert(){
    is_there_root_decks || { echo "Please insert at least one deck before including a card."; return 1; }

    printf "Insertion ends with an empty line.\n"
    boldprint "SideA (Question):\n"; sideA=$(get_multiline_text)
    boldprint "SideB (Answer):\n"; sideB=$(get_multiline_text)
    deck=$(decksearch -a 'New deck…' -p "Select deck for '$sideA': ")
    
    if [ "$deck" == 'New deck…' ]; then
        deck_multiple_insert
        deck=$(decksearch -p "Select deck for '$sideA': ")
        echo ""
    fi
    
    boldprint "Deck: " "$deck\n"
    
    sideA=${sideA//\"/ʺ}
    
    recins --verbose -t Card \
        -f SideA -v "$sideA" \
        -f SideB -v "$sideB" \
        -f Deck -v "$deck" \
        -f Type -v onesided \
        -f Box -v 1 \
        "$flashcardsfile"
}
# -------------------------------------------------------------------------------------------------}}}
# Card move ---------------------------------------------------------------------------------------{{{
cardmove(){
    is_there_root_decks || { echo "Please insert at least one root deck."; return 1; }

    old_deck=$(decksearch -p "Where is the card you want to move?")
    cards_in_old_deck=$(cards_in_deck "$old_deck" --collapse)

    if [ "$cards_in_old_deck" ]
    then
        prompt="Select card in '$old_deck':"
        selected_card=$(echo "$cards_in_old_deck" | fzf --algo=v1 --reverse --header="$prompt")

        prompt="Where to put '$selected_card' (before, in '$old_deck'):"
        new_deck=$(decksearch -p "$prompt")

        echo "Moving: $selected_card: $old_deck →  $new_deck"

        if [ "$old_deck" = "$new_deck" ]
        then
            echo "New deck is the same as old deck. Doing nothing."
            return 0
        else
            local s r
            s=$'\n' r=$'\u2028 '
            decollapsed_card="${selected_card//$r/$s}"

            recset -e "SideA = \"$decollapsed_card\"" \
                -f Deck -s "$new_deck" "$flashcardsfile" \
                -t Card \
                && echo "Moved!"
        fi
    else
        echo "No cards in $old_deck".
    fi
}
# -------------------------------------------------------------------------------------------------}}}
# Review ------------------------------------------------------------------------------------------{{{
# TODO: add Retired Cards box and logic

genstackfile(){
    {
        echo "%rec: CardsDueToday"

        recsel -e "Box = '1'" -p SideA -t Card "$flashcardsfile"
        if [ $(( lastcycle % 2 )) == 0 ]; then
            recsel -e "Box = '2'" -p SideA -t Card "$flashcardsfile"
        fi
        if [ $(( lastcycle % 7 )) == 0 ]; then
            recsel -e "Box = '3'" -p SideA -t Card "$flashcardsfile"
        fi
    } | sed 's/SideA/\nCard/g' > "$stackfile"
    if [[ $(cardcount) -eq 0 ]]
    then
        echo "No cards due today."
        return 0
    fi
}

getcard(){
    recsel -P Card -m 1 -t CardsDueToday "$stackfile"
}

popcard(){
    recdel -e "Card = \"$1\"" -t CardsDueToday "$stackfile"
}

updatecycle(){
    recset --verbose -t ReviewingCycle -f LastCycle -s $((lastcycle +1)) "$flashcardsfile"
}

cardcount(){
    grep -c -e '^Card:' "$stackfile"
}

promote_demote_card(){
    if [ $2 -le 3 ]; then
        recset --verbose -t Card -e "SideA = \"$1\"" -f Box -s "$2" "$flashcardsfile"
    fi
}

print_card_side(){
    echo
    echo "$1" | fold -s -w $(( COLUMNS -1 )) | sed 's/^/ /g'
}

do_review(){
    while [[ $(cardcount) -ne 0 ]]; do
        question=$(getcard)
        deck=$(recsel -e "SideA = \"$question\"" -P Deck -t Card "$flashcardsfile")
        answer=$(recsel -e "SideA = \"$question\"" -P SideB -t Card "$flashcardsfile")

        bold_echo "Deck:" "$deck"
        bold_echo "Question:" "$(print_card_side "$question")"
        read -r -p "Try to remember the answer. Then press Return to continue."
        bold_echo "Answer:" "$(print_card_side "$answer")"

        review_prompt[0]='Please type [y/n]: '
        review_prompt[1]='Did you remember the answer correctly and with ease? [y/n]: '

        unset yn
        unset p
        until [[ ${yn,,} == [yn] ]]; do
            read -r -p "${review_prompt[!p++]}" yn
        done

        case ${yn,,} in
            y) box=$(recsel -e "SideA = \"$question\"" -P Box -t Card "$flashcardsfile")
               promote_demote_card "$question" $((box + 1))
               echo "Card promoted."
               ;;
            n) promote_demote_card "$question" 1
               echo "Then let's review it more often."
               ;;
        esac

        popcard "$question"
        echo ""
    done
    echo "Finished."
    updatecycle
}

review(){
    # If stackfile doesn't exist, generates it, and start revision
    [ -f "$stackfile" ] || {
        genstackfile
        do_review
        return 0
    }

    # Else, check cardcount
    if [[ $(cardcount) -eq 0 ]]
    then
        if [ "$(LANG=en stat "$stackfile" | grep Modify | awk '{print $2}')" \
            == "$(date +%Y-%m-%d)" ]
        then
            echo "Perhaps you already finished your revision for today :)"
            echo "Please come back tomorrow!"
        else
            # Modified yesterday or before. Then generates a new one for today.
            genstackfile
            do_review
        fi
    else
        # Cards left on queue
        echo "Resuming revision..."
        echo ""
        do_review
    fi
}
# -------------------------------------------------------------------------------------------------}}}

firstheader='
    ,_   _  _  |\ |\  _,    ,  |)
   /  | |/ /   |/ |/ / |   / \_|/\
      |/|_/\__/|_/|_/\/|_/o \/ |  |/
               |)
'

menu_header(){
    dashstring=
    dashcounter=0
    until [ $dashcounter = $2 ]
    do
        dashstring=$dashstring-
        dashcounter=$((dashcounter+1))
    done
    printf "%s " "$dashstring"

    # shellcheck disable=SC2001
    echo "$1" | sed 's/./& /g'
}

menu(){
    for (( i=1; i<=$#; i++ ))
    do
       echo "$i. ${!i}"
    done | column -s $'\n' -c $(( COLUMNS * 2 / 5 ))

    read -re -p 'Enter a number: '
    [[ "$REPLY" =~ ^[1-"$#"]$ ]] || return 1
}

main(){
    echo "$firstheader"

    until [ "$option" = 'Quit' ]
    do
        menu_header 'main menu' 3

        options=('Explore' 'Review' 'Insert card' 'Move card' 'Insert deck' 'Move deck' 'Quit')
        menu "${options[@]}" || { echo 'Invalid option!'$'\n'; continue; }
        option=${options[$REPLY-1]}

        case "$option" in
            'Explore')
                (decksearch -p 'Exploring decks')
                # Notice: this subshell is "needed" because there's a bug.
                # If we call this line before any call for decksearch with -a, the added entry
                # for fzf would not show. Hopefully, I'll someday find out a better solution
                # than isolating these command in subshells.
                ;;
            'Review')
                echo ""
                menu_header 'review' 2
                (review)
                ;;
            'Insert card')
                echo ""
                menu_header 'insert card' 2
                (cardinsert)
                ;;
            'Move card')
                echo ""
                menu_header 'move card' 2
                (cardmove)
                ;;
            'Insert deck')
                echo ""
                menu_header 'insert deck' 2
                (deck_multiple_insert)
                ;;
            'Move deck')
                echo ""
                menu_header 'move deck' 2
                (deckmove)
                ;;
            'Quit')
                echo ""
                echo "Bye!"
                ;;
        esac
        echo ""
    done    # until
}

main

# vim: foldmethod=marker
