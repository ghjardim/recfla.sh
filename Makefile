SHELL = /bin/sh

all:
	@echo Run \'make install\' to install recfla.sh.
	@echo Run \'make remove\' to uninstall recfla.sh, while keeping configuration files.
	@echo Run \'make purge\' to uninstall recfla.sh, and remove configuration files.

install:
	mkdir -p /usr/local/bin
	cp -p recfla.sh /usr/local/bin/recfla.sh
	mkdir -p /usr/local/etc/recfla.sh
	cp -p examples/empty.rec /usr/local/etc/recfla.sh/flash.rec
	cp -p examples/recflashrc /usr/local/etc/recfla.sh/recflashrc

remove:
	rm -rf /usr/local/bin/recfla.sh

purge:
	rm -rf /usr/local/bin/recfla.sh
	rm -rf /usr/local/etc/recfla.sh
